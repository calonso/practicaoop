
class Empleado:
    def __init__(self,n,s):
        self.nombre=n
        self.sueldo=s
    def Impuestos(self):
        imp=self.sueldo*0.3
        return imp
    def __str__(self):
       return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.Impuestos())
    
empleadoPepe = Empleado ('Pepe',20000)
empleadoAna = Empleado('Ana',30000)

total = empleadoPepe.Impuestos() + empleadoAna.Impuestos()
print(empleadoPepe)
print(empleadoAna)
print("Los impuestos a pagar en total son {:.2f} euros".format(total))