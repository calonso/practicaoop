import unittest
class Empleado:
    def __init__(self,n,s):
        self.nombre=n
        self.sueldo=s
    def Impuestos(self):
        imp=self.sueldo*0.3
        return imp
    def __str__(self):
       return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.Impuestos())
    def test_str(self):
        e1=Empleado("Pepe",20000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())
        
if __name__ == "__main__":
 unittest.main()